public class RadixSort implements Sorter {

	private static final int RADIX = 12;
	
	private static final int LENGTH_OF_COUNT = (int) Math.pow(2, RADIX);	
	
	public void sort(double[] arr)
	{
		//for every index of our radix
		for(int i = 0; i < 64 / RADIX; i++)
		{
			countingSort(arr,i);
		}
	}
	
	private void countingSort(double[] arr, int iter)
	{
		//Create a counting array and fill it with zeros
		int[] count = new int[LENGTH_OF_COUNT];
		
		//mask to get the relevent bits
		long mask = 0;
		for(int i = 0; i < RADIX; i++)
		{
			mask <<=1;
			mask++;
		}
		mask <<= (RADIX*iter);

		//Because we cant memory manage right in java, we have to do some hacks
		double[] bk = arr.clone();

		//Get the least significant last RADIX digits and count them
		for(int i = 0; i < arr.length; i++)
		{
			//Turn our double into something we can work with via bitwise
			long lkey = Double.doubleToLongBits(bk[i]) & mask;
			int key = (int) (lkey >>> (RADIX*iter));

			//Mod for case where we have no value
			count[key]++;
		}

		//Add up the numbers in the counting array
		for(int i = 1; i < count.length; i++)
			count[i] += count[i-1];

		//Put values into place
		for(int i = arr.length-1; i >= 0; i--)
		{
			long lkey = Double.doubleToLongBits(bk[i]) & mask;
			int key = (int) (lkey >>> (RADIX*iter));
			
			arr[count[key]-1] = bk[i];
			count[key]--;
		}

		//Cry your self to sleep because of radix on doubles.
	}
}
